import ApolloClient, { gql } from 'apollo-boost';

global.ApolloClient = ApolloClient;
global.gql = gql;
